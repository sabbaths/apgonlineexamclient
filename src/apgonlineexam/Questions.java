/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apgonlineexam;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Alex
 */
public class Questions {
    static int[] solutionArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    static int item = 0;
    static String[][] questionsArray = {
            {"C152 number of crew?", 
                "1", 
                "2", 
                "3",
                "4",
                "A"},
            {"C152 capacity?", 
                "2 pax", 
                "1 pax", 
                "3 pax", 
                "4 pax", 
                "B"},
            {"C152 Engine?", 
                "AVCO LYCOMING", 
                "NARCO LYCOMING", 
                "4age",
                "Turbocharge",
                "A"},
            {"Wingspan c152?", 
                "32 ft 4", 
                "24 ft", 
                "33 ft 4",
                "39 ft",
                "C"},
            {"How Many Magnetos in 152?", 
                "2", 
                "1", 
                "3", 
                "4", 
                "A"},
            {"What type of Fuel System is the c152?", 
                "Gravity", 
                "Pump", 
                "Diesel",
                "Gas",
                "A"},
            {"Fuel Type c152?", 
                "a1 jet fuel", 
                "avgas 100",
                "mogas",
                "avgas90",
                "B"},
            {"Color of Fuel c152?", 
                "Blue", 
                "Red",
                "Pink",
                "Pinkish",
                "B"},
            {"c152 Landing Gear type?", 
                "Fixed", 
                "Retractable",
                "Tailwheel",
                "Conventional",
                "A"},
            {"Power Setting for cruise: c152?", 
                "2300", 
                "2400",
                "2000",
                "2500",
                "A"},
            {"Entry Procedure for Climb?", 
                "P.P.T", 
                "P.T.P",
                "Power Pitch Trim",
                "Pitch Power Trim",
                "C"},
            {"Vx Speed c152?", 
                "54", 
                "55",
                "67",
                "65",
                "A"},
            {"Vy Speed c152?", 
                "67", 
                "65",
                "60",
                "59",
                "A"},
            {"Height c152?", 
                "8 ft 6", 
                "6 ft 8",
                "7 ft 5",
                "8 ft",
                "B"},
            {"Empty weight c152?", 
                "1081lb", 
                "1000lb",
                "1900lb",
                "1200lb",
                "B"},
            {"Type of Wing Support c152?", 
                "Semi Cantilever", 
                "Cantilever",
                "Fixed",
                "Sweepback",
                "A"},
            {"Landing Length c152?", 
                "725", 
                "475",
                "1340",
                "1200",
                "B"},
            {"Takeoff Length c152?", 
                "424", 
                "725",
                "1344",
                "1200",
                "B"},
            {"Runway length subic?", 
                "9004ft", 
                "2000m",
                "9000ft",
                "2000ft",
                "A"},
            {"Traffic pattern altitude RPLB?", 
                "1000", 
                "1,200",
                "700",
                "800",
                "C"},    
            {"Type of Brakes c152?", 
                "Differential", 
                "Lever",
                "Aerodyamic",
                "No Brakes",
                "A"},              
    };
    
    String question = "Question";
    String answerA = "A";
    String answerB = "B";
    String answerC = "C";
    String answerD = "D";
    /*
    public static void main(String[] args) {
        Questions();
        System.out.println(next()[0]);
        System.out.println(next()[0]);
    } */
    
    public Questions() {
        this.setNumber();
        shuffleArray(solutionArray);
        for (int i = 0; i < solutionArray.length; i++)
        {
          System.out.print(solutionArray[i] + " ");
        }
        System.out.println();
    }
    
    static void shuffleArray(int[] ar) {
    // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }
    
    static String[] next() {
        int number = solutionArray[item];
        String sq = questionsArray[number][0];
        String a = questionsArray[number][1];
        String b = questionsArray[number][2];
        String c = questionsArray[number][3];
        String d = questionsArray[number][4];
        String ans = questionsArray[number][5];
        item++;
        String [] ret = {sq, a, b, c, d, ans};
        return ret;
    }
    
    static int getNumber() {
        return item;
    }
    
    static void setNumber() {
        item = 0;
    }
    
}
