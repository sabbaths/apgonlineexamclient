/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apgonlineexam;

import java.awt.Color;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Alex
 */
public class Main extends javax.swing.JFrame {
    public StudentExam student_exam;
    
    private ConnectWebServer cs;
    private TreeMap <Integer, String> subjMap = new TreeMap<Integer, String>();
    private TreeMap <Integer, String> subjMapTimeLimit = new TreeMap<Integer, String>();
    private TreeMap <Integer, String> studentsMap = new TreeMap<Integer, String>();
    private TreeMap <Integer, String> coursesMap = new TreeMap<Integer, String>();
    private TreeMap <Integer, String> instructorsMap = new TreeMap<Integer, String>();
    /**
     * Creates new form Main
     */
    public Main() {

        
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setUndecorated(true);
        setResizable(false);
        this.getContentPane().setBackground(Color.WHITE);
        initComponents();
        initComboBox();
        //this.setIconImage("../images/apg_logo.jpeg");
        //this.setIconImage(new imageIcon(getClass().getClassLoader().getResource("PlagiaLyzerIcon.png")));
        
        try {
            String course_select = jComboBoxCourses.getSelectedItem().toString();
            int courseID = getKey(coursesMap, course_select);
            this.getSubjects(courseID);
        } catch (Exception e) {
            
        }
    }
    
    private String getServerURL(String fileName) {
        return "";
    }
    
    public void initComboBox() {
        cs = new ConnectWebServer();
        this.getCourses();
        this.getStudents();
        this.getInstructors();
        this.getSubjects();
    }
    /*
    private void getCourses() {
        try {
            jComboBoxCourses.removeAllItems();
            JSONObject courses =  cs.getServerCourses();
            JSONArray arr = courses.getJSONArray("courses"); 
            for (int i = 0; i < arr.length(); i++) {
                String course_name = arr.getJSONArray(i).getString(1);
                jComboBoxCourses.addItem(course_name);
            }            
        } catch (Exception e) {
            System.out.println("getCourses:" + e);
        }
    } */
    
    private void getCourses() {
        try {
            jComboBoxCourses.removeAllItems();
            JSONObject courses =  cs.getFromServerList(cs.getServerURL("courses.php"));;
            JSONArray arr = courses.getJSONArray("courses"); 
            for (int i = 0; i < arr.length(); i++) {
                int course_id = arr.getJSONArray(i).getInt(0);
                String course_name = arr.getJSONArray(i).getString(1);
                coursesMap.put(course_id, course_name);
                jComboBoxCourses.addItem(course_name);
            }            
        } catch (Exception e) {
            System.out.println("getCourses:" + e);
        }
    }
        
    
    private void getStudents() {
        try {
            jComboBoxStudent.removeAllItems();
            JSONObject courses =  cs.getFromServerList(cs.getServerURL("students_list.php"));
            JSONArray arr = courses.getJSONArray("students"); 
            for (int i = 0; i < arr.length(); i++) {
                int student_id = arr.getJSONArray(i).getInt(0);
                String first_name = arr.getJSONArray(i).getString(1);
                String middle_name = arr.getJSONArray(i).getString(2);
                String last_name = arr.getJSONArray(i).getString(3);
                String full_name = first_name + " " + middle_name + " " + last_name;
                studentsMap.put(student_id, full_name);
                
                jComboBoxStudent.addItem(full_name);
            }            
        } catch (Exception e) {
            System.out.println("getCourses:" + e);
        }   
    }
    
    private void getInstructors() {
        try {
            jComboBoxGI.removeAllItems();
            JSONObject courses =  cs.getFromServerList(cs.getServerURL("instructors_list.php"));;
            JSONArray arr = courses.getJSONArray("instructors"); 
            for (int i = 0; i < arr.length(); i++) {
                int instructor_id = arr.getJSONArray(i).getInt(0);
                String first_name = arr.getJSONArray(i).getString(1);
                String middle_name = arr.getJSONArray(i).getString(2) != null ? arr.getJSONArray(i).getString(2) : "";
                String last_name = arr.getJSONArray(i).getString(3);
                String full_name = first_name + " " + middle_name + " " + last_name;
                instructorsMap.put(instructor_id, full_name);
                
                jComboBoxGI.addItem(full_name);
            }            
        } catch (Exception e) {
            System.out.println("getInstructors:" + e);
        }   
    }
    
    private void getSubjects() {
        
        try {
            jComboBoxSubject.removeAllItems();
            JSONObject courses =  cs.getFromServerList(cs.getServerURL("subjects_list.php"));;
            JSONArray arr = courses.getJSONArray("subjects"); 
            for (int i = 0; i < arr.length(); i++) {
                int subj_id = arr.getJSONArray(i).getInt(0);
                String subj_code = arr.getJSONArray(i).getString(1);
                String subj_name = arr.getJSONArray(i).getString(2);
                subjMap.put(subj_id, subj_name);
                
                jComboBoxSubject.addItem(subj_name);
            }            
        } catch (Exception e) {
            System.out.println("getCourses:" + e);
        }   
    }
    
    private void getSubjects(int course_id) {
        
        try {
            jComboBoxSubject.removeAllItems();
            JSONObject courses =  cs.getFromServerList(cs.getServerURL("subjects_list.php"), "course_id="+course_id);
            JSONArray arr = courses.getJSONArray("subjects"); 
            for (int i = 0; i < arr.length(); i++) {
                int subj_id = arr.getJSONArray(i).getInt(0);
                String subj_code = arr.getJSONArray(i).getString(1);
                String subj_name = arr.getJSONArray(i).getString(2);
                String subj_time_limit = arr.getJSONArray(i).getString(3);
                subjMap.put(subj_id, subj_name);
                subjMapTimeLimit.put(subj_id, subj_time_limit);
                
                jComboBoxSubject.addItem(subj_name);
            }            
        } catch (Exception e) {
            System.out.println("getCourses:" + e);
        }   
    }
    
    static Integer getKey(TreeMap<Integer, String> map, String value) {
        Integer key = null;
        for(Map.Entry<Integer, String> entry : map.entrySet()) {
            if((value == null && entry.getValue() == null) || (value != null && value.equals(entry.getValue()))) {
                key = entry.getKey();
                break;
            }
        }
        return key;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jComboBoxSubject = new javax.swing.JComboBox<>();
        jComboBoxGI = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jToggleButtonExit = new javax.swing.JToggleButton();
        jToggleButtonStart = new javax.swing.JToggleButton();
        jLabel5 = new javax.swing.JLabel();
        jComboBoxCourses = new javax.swing.JComboBox<>();
        jComboBoxStudent = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setMinimumSize(new java.awt.Dimension(500, 500));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jComboBoxSubject.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jComboBoxSubject.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pre-Solo", "Aerodyamics", "Basic Aircraft Instruments" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 44;
        gridBagConstraints.ipady = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jComboBoxSubject, gridBagConstraints);

        jComboBoxGI.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jComboBoxGI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Capt. Parthik", "Capt. Joms", "Capt. Prashant" }));
        jComboBoxGI.setPreferredSize(new java.awt.Dimension(261, 20));
        jComboBoxGI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxGIActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 44;
        gridBagConstraints.ipady = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jComboBoxGI, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setText("Subject:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 41;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 0, 0);
        jPanel1.add(jLabel1, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel2.setText("Instructor:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 41;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 0, 0);
        jPanel1.add(jLabel2, gridBagConstraints);

        jToggleButtonExit.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jToggleButtonExit.setText("Exit");
        jToggleButtonExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonExitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        jPanel1.add(jToggleButtonExit, gridBagConstraints);

        jToggleButtonStart.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jToggleButtonStart.setLabel("Start");
        jToggleButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonStartActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.weightx = 3.2;
        jPanel1.add(jToggleButtonStart, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel5.setText("Course:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 41;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 0, 0);
        jPanel1.add(jLabel5, gridBagConstraints);

        jComboBoxCourses.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jComboBoxCourses.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pre-Solo", "Aerodyamics", "Basic Aircraft Instruments" }));
        jComboBoxCourses.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxCoursesItemStateChanged(evt);
            }
        });
        jComboBoxCourses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCoursesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 44;
        gridBagConstraints.ipady = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jComboBoxCourses, gridBagConstraints);

        jComboBoxStudent.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jComboBoxStudent.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pre-Solo", "Aerodyamics", "Basic Aircraft Instruments" }));
        jComboBoxStudent.setName("comboCourse"); // NOI18N
        jComboBoxStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxStudentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 44;
        gridBagConstraints.ipady = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jComboBoxStudent, gridBagConstraints);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel7.setText("Student:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 0, 0);
        jPanel1.add(jLabel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 246;
        gridBagConstraints.insets = new java.awt.Insets(11, 10, 11, 10);
        getContentPane().add(jPanel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        getContentPane().add(jLabel3, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel4.setText("APGIAA Online Exam");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        getContentPane().add(jLabel4, gridBagConstraints);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/apg_logo.jpeg"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        getContentPane().add(jLabel8, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButtonExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonExitActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jToggleButtonExitActionPerformed

    private void jToggleButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonStartActionPerformed
        this.student_exam = new StudentExam();
        String course_select = jComboBoxCourses.getSelectedItem().toString();
        String gi_select = jComboBoxGI.getSelectedItem().toString();
        String subj_select = jComboBoxSubject.getSelectedItem().toString();
        String stud_select = jComboBoxStudent.getSelectedItem().toString();        
        
        int courseID = getKey(coursesMap, course_select);
        int giID = getKey(instructorsMap, gi_select);
        int subjID = getKey(subjMap, subj_select);
        int studID = getKey(studentsMap, stud_select);
        int subj_time_limit = Integer.parseInt(subjMapTimeLimit.get(subjID));
        int userID = 0;
        String userName = "";
        System.out.println("SUBJECT TIME LIMIT:" + subj_time_limit);
        
        try {
            userID = Integer.parseInt((String) cs.getUserJSON().get("user_id"));
            userName = (String) cs.getUserJSON().get("username");
        } catch (JSONException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
            

        
        this.student_exam.setCourse_id(courseID);
        this.student_exam.setInstructor_id(giID);
        this.student_exam.setSubject_id(subjID);
        this.student_exam.setStudent_id(studID);
        this.student_exam.setCourse_name(course_select);
        this.student_exam.setInstructor_name(gi_select);
        this.student_exam.setSubject_name(subj_select);
        this.student_exam.setStudent_name(stud_select);
        this.student_exam.setUser_id(userID); //get user id
        this.student_exam.setUser_name(userName); //get user name
        this.student_exam.setSubject_time_limit(subj_time_limit);
        

            //Exam exam = new Exam();
            //exam.setVisible(true);

        jToggleButtonStart.setSelected(false);
        //check if still doing exam
        //check if there are questions for the subject
        JSONObject studentExamJSON = cs.generateExam(studID, courseID, subjID, giID, userID); //userid
        System.out.println("FROM SERVER studnet exam json: " + studentExamJSON);
        int studentExamID = 0;
        int status_code = 3002;
        try {
            status_code = studentExamJSON.getInt("status_code");
            studentExamID = studentExamJSON.getInt("student_exam_id");
        } catch (JSONException ex) {
            System.out.println("EXCEPTION GET STUDENT EXAM ID" + ex);
        }
        if(status_code == 3003) {
            JOptionPane optionPane = new JOptionPane();
            optionPane.setMessage("NO AVAILABLE QUESTIONS FOR THIS SUBJECT: CONTACT ADMIN");
            JDialog dialog = optionPane.createDialog("STATUS");
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);  
            return;
        } else if (status_code == 3002) {
            JOptionPane optionPane = new JOptionPane();
            optionPane.setMessage("System will automatically continue interrupted exam for this student.");
            JDialog dialog = optionPane.createDialog("STATUS");
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);  
            
            ExamPage exam = new ExamPage(this.student_exam, studentExamID, studID, courseID, subjID, giID, userID); //userid);
            exam.setVisible(true);
            this.setVisible(false); 
            return;
        } else {
            int eq_status_code = cs.generateExamQuestions(studID, courseID, subjID, giID, userID, studentExamID, 30); //limit

            if(eq_status_code == 4001) {
                ExamPage exam = new ExamPage(this.student_exam, studentExamID, studID, courseID, subjID, giID, userID); //userid);
                exam.setVisible(true);
                this.setVisible(false);         
            } else { //4004 error creating exam questions
                System.exit(0);
            }            
        }
        

    }//GEN-LAST:event_jToggleButtonStartActionPerformed

    private void jComboBoxGIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxGIActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxGIActionPerformed

    private void jComboBoxCoursesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCoursesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxCoursesActionPerformed

    private void jComboBoxStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxStudentActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jComboBoxStudentActionPerformed

    private void jComboBoxCoursesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxCoursesItemStateChanged
        // TODO add your handling code here:

        try {
            String course_select = jComboBoxCourses.getSelectedItem().toString();
            int courseID = getKey(coursesMap, course_select);
            this.getSubjects(courseID);
        } catch (Exception e) {
            
        }

    }//GEN-LAST:event_jComboBoxCoursesItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jComboBoxCourses;
    private javax.swing.JComboBox<String> jComboBoxGI;
    private javax.swing.JComboBox<String> jComboBoxStudent;
    private javax.swing.JComboBox<String> jComboBoxSubject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToggleButton jToggleButtonExit;
    private javax.swing.JToggleButton jToggleButtonStart;
    // End of variables declaration//GEN-END:variables
}
