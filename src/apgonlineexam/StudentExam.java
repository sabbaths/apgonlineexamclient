/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apgonlineexam;

/**
 *
 * @author sabbaths
 */
public class StudentExam {

    public static int getSubject_time_limit() {
        return subject_time_limit;
    }

    public static void setSubject_time_limit(int subject_time_limit) {
        StudentExam.subject_time_limit = subject_time_limit;
    }

    public static String getStudent_name() {
        return student_name;
    }

    public static void setStudent_name(String student_name) {
        StudentExam.student_name = student_name;
    }

    public static String getCourse_name() {
        return course_name;
    }

    public static void setCourse_name(String course_name) {
        StudentExam.course_name = course_name;
    }

    public static String getSubject_name() {
        return subject_name;
    }

    public static void setSubject_name(String subject_name) {
        StudentExam.subject_name = subject_name;
    }

    public static String getUser_name() {
        return user_name;
    }

    public static void setUser_name(String user_name) {
        StudentExam.user_name = user_name;
    }

    public static String getInstructor_name() {
        return instructor_name;
    }

    public static void setInstructor_name(String instructor_name) {
        StudentExam.instructor_name = instructor_name;
    }

    public static int getStudent_exam_id() {
        return student_exam_id;
    }

    public static void setStudent_exam_id(int student_exam_id) {
        StudentExam.student_exam_id = student_exam_id;
    }

    public static int getCourse_id() {
        return course_id;
    }

    public static void setCourse_id(int course_id) {
        StudentExam.course_id = course_id;
    }

    public static int getInstructor_id() {
        return instructor_id;
    }

    public static void setInstructor_id(int gi_id) {
        StudentExam.instructor_id = gi_id;
    }

    public static int getSubject_id() {
        return subject_id;
    }

    public static void setSubject_id(int subject_id) {
        StudentExam.subject_id = subject_id;
    }

    public static int getStudent_id() {
        return student_id;
    }

    public static void setStudent_id(int student_id) {
        StudentExam.student_id = student_id;
    }

    public static int getUser_id() {
        return user_id;
    }

    public static void setUser_id(int user_id) {
        StudentExam.user_id = user_id;
    }
    private static String course_name;
    private static String subject_name;
    private static String user_name;
    private static String instructor_name;
    private static String student_name;
    private static int student_exam_id;
    private static int course_id;
    private static int instructor_id;
    private static int subject_id;
    private static int student_id;
    private static int user_id;
    private static int subject_time_limit;
}
