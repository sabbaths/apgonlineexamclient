/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apgonlineexam;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Alex
 */
public class ConnectWebServer {
    private static boolean is_jar_file = false;
    private static String port = "";
    private static String server = "";
    private static String url = "";
    private static String whole_site_name = "";
    private static String pdf_location = "";
    private static int user_id;
    private static JSONObject user_json_arr;
    
    public ConnectWebServer() {
    }
    
    public JSONObject getUserJSON() {
        return this.user_json_arr;
    }
    
    public String getUser() {
        return "";
    }

    public void getConfigFile() {
        try {
            JSONParser parser = new JSONParser();

            if(this.is_jar_file) {
                Object obj = parser.parse(new FileReader("config.json"));
                org.json.simple.JSONObject jsonObject =  (org.json.simple.JSONObject) obj;              
                
                this.port = jsonObject.get("port").toString();
                this.server = jsonObject.get("server").toString();
                this.url = jsonObject.get("url").toString();
                this.pdf_location = jsonObject.get("pdf").toString();
                this.whole_site_name = "http://"+this.url+":"+this.port+"/"+this.server;
                int status = servletCheckConnection();
                //JOptionPane.showMessageDialog(null, "SERVER JAR: " + whole_site_name + " status: "+ status);
            } else {
                Object obj = parser.parse(new FileReader("./src/config/config.json"));
                org.json.simple.JSONObject jsonObject =  (org.json.simple.JSONObject) obj; 
                
                this.port = jsonObject.get("port").toString();
                this.server = jsonObject.get("server").toString();
                this.url = jsonObject.get("url").toString();
                this.pdf_location = jsonObject.get("pdf").toString();
                this.whole_site_name = "http://"+this.url+":"+this.port+"/"+this.server;
                int status = servletCheckConnection();
                //JOptionPane.showMessageDialog(null, "SERVER JAR: " + whole_site_name + " status: "+ status);
            }          
        } catch (FileNotFoundException e) {
            //create config file via configframe
            
            ConfigFrame cf = new ConfigFrame();
            cf.setVisible(true);
            
            System.exit(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "EXCEPTION: " + e);
            System.exit(0);
        }
    }
    
    public String getPDFLocation() {
        return this.pdf_location;
    }
    
    public String getServerURL(String phpFile) {
        return "http://"+this.url+":"+this.port+"/"+this.server+"/"+phpFile;
    }
    
    public void sendPost() throws Exception {
        String url = "https://selfsolve.apple.com/wcResults.do";
        URL obj = new URL(this.getServerURL(""));
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        //con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "username=C02G8416DRJM&cn=&locale=&caller=&num=12345";

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }
    
    public int servletLogin(String username, String password) throws Exception {
        int ret = 99;

        try {        
            String param = "username="+username+"&password=" +password;
            URL url = new URL(getServerURL("login.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            System.out.println("OUTPUT: " + connection.getResponseMessage());
            System.out.println("OUTPUT: " + connection.getResponseCode());
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
            
            String jsonData = br.readLine();
            System.out.println(jsonData);
            JSONObject jsonObject = new JSONObject(jsonData);
            jsonObject.getInt("status_code");
            int status_code_return = jsonObject.getInt("status_code");
            System.out.println("OUTPUT LOGIN: " + jsonObject.getInt("status_code"));
            ret = status_code_return;
            JSONObject obj =  (JSONObject) jsonObject.get("user");
            this.user_json_arr = obj;//obj.get("user"); 
        } catch (Exception e) {
            System.out.println("Exception Login: " + e);
        }   
        
        return ret;
    }    
    
    public int servletCheckConnection() {
        int return_status_code = 404;
        
        try {
            int responseCode = 200;
            String param = "str=YOURSTRING";
            URL url = new URL(this.getServerURL(""));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", "" +  Integer.toString(param.getBytes().length));
            connection.setUseCaches (false);

            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            return_status_code = connection.getResponseCode();
            System.out.println("OUTPUT: " + return_status_code);
            out.writeBytes(param);
            out.flush();
            out.close();
            connection.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return return_status_code;
    }
    
    public JSONObject getFromServerList(String HTTPurl) {
        JSONObject jsonObject = new JSONObject();
        try {        
            
            URL url = new URL(HTTPurl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER:" + jsonData);
            jsonObject = new JSONObject(jsonData);
            //System.out.println("OUTPUT LOGIN: " + jsonObject.getInt("status_code"));
            
        } catch (Exception e) {
            System.out.println("Exception Login: " + e);
        }   
        
        return jsonObject; 
    }
    
    public JSONObject getFromServerList(String HTTPurl, String params) {
        JSONObject jsonObject = new JSONObject();
        try {        
            
            URL url = new URL(HTTPurl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(params);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER:" + jsonData);
            jsonObject = new JSONObject(jsonData);
            //System.out.println("OUTPUT LOGIN: " + jsonObject.getInt("status_code"));
            
        } catch (Exception e) {
            System.out.println("Exception Login: " + e);
        }   
        
        return jsonObject; 
    }
    
    public JSONObject generateExam(int studentID, int courseID, int subjectID, int instructorID, int userID) {
        //add checking if still doing exam
        JSONObject jsonObject = new JSONObject();
        try {        
            String param = "student_id="+studentID+
                    "&course_id=" +courseID+
                    "&subject_id="+subjectID+
                    "&instructor_id="+instructorID+
                    "&user_id="+userID;
            
            URL url = new URL(getServerURL("generate_exam.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER GENERATE EXAM:" + jsonData);
            JSONObject jsonObject1 = new JSONObject(jsonData);
            //int student_exam_id = jsonObject1.getInt("inserted_row_id");
            return jsonObject1;
        } catch (Exception e) {
            System.out.println("Exception generate exam: " + e);
        }  
        
        return jsonObject; 
    }

    int generateExamQuestions(int studentID, int courseID, int subjectID, 
            int instructorID, int userID, int studentExamID, int limit) {
        //add checking if still doing exam
        JSONObject jsonObject = new JSONObject();
        try {        
            String param = "student_id="+studentID+
                    "&course_id=" +courseID+
                    "&subject_id="+subjectID+
                    "&instructor_id="+instructorID+
                    "&student_exam_id="+studentExamID+
                    "&limit="+limit+
                    "&user_id="+userID;
            
            URL url = new URL(getServerURL("generate_exam_questions.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER GENERATE EXAM QUESTIONS:" + jsonData);
            JSONObject jsonObject1 = new JSONObject(jsonData);
            return jsonObject1.getInt("status_code");
        } catch (Exception e) {
            System.out.println("Exception generate exam: " + e);
        }  
        
        return 4004; 
    }
    
    JSONObject getNextQuestion(int student_exam_id) {
        JSONObject jsonObject = new JSONObject();
        try {        
            String param = "student_exam_id="+student_exam_id;
            
            URL url = new URL(getServerURL("get_next_question.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
            System.out.println("STUDNET EXAM ID: " + student_exam_id);
            String jsonData = br.readLine();
            System.out.println("FROM SERVER GET NEXT QUESTION:" + jsonData);
            JSONObject jsonObject1 = new JSONObject(jsonData);
            return jsonObject1;
        } catch (Exception e) {
            System.out.println("Exception generate exam: " + e);
        }  
        
        return jsonObject; 
    }
    
    JSONObject updateExamQuestion(int exam_id, int is_correct) {
        JSONObject jsonObject = new JSONObject();
        try {        
            String param = "exam_id="+exam_id+"&is_correct="+is_correct;
            
            URL url = new URL(getServerURL("update_exam_question.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER UPDATE EXAM QUESTION:" + jsonData);
            JSONObject jsonObject1 = new JSONObject(jsonData);
            return jsonObject1;
        } catch (Exception e) {
            System.out.println("Exception update exam question: " + e);
        }  
        
        return jsonObject; 
    }
    
    public JSONObject getExamScore(int student_exam_id) {
        JSONObject jsonObject = new JSONObject();
        try {        
            String param = "student_exam_id="+student_exam_id;
            
            URL url = new URL(getServerURL("get_exam_score.php"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();          
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            DataOutputStream out = new DataOutputStream(connection.getOutputStream ());
            out.writeBytes(param);
            out.flush();
            out.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String jsonData = br.readLine();
            System.out.println("FROM SERVER GET EXAM SCORE:" + jsonData);
            JSONObject jsonObject1 = new JSONObject(jsonData);
            return jsonObject1;
        } catch (Exception e) {
            System.out.println("Exception update exam question: " + e);
        }  
        
        return jsonObject; 
    }
}
