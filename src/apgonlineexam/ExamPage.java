/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apgonlineexam;

import java.awt.Color;
import java.awt.Font;
import java.util.StringTokenizer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.text.DefaultCaret;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Alex
 */
public class ExamPage extends javax.swing.JFrame {
    Questions questions;
    String correctAnswer = "";
    int correctAnswerCount = 0;
    int total = 20;
    private int student_exam_id;
    private ConnectWebServer cs;
    String question;
    String choice_a;
    String choice_b;
    String choice_c;
    String choice_d;
    String answer;
    String answer_letter;
    int exam_id;
    
    public StudentExam student_exam;
    
    private static int student_id;
    private static int course_id;
    private static int subject_id;
    private static int gi_id;
    private static int user_id;
    private static int question_number;
    private static int subject_time_limit;
    

    /**
     * Creates new form Absolute
     */
    public ExamPage(int student_exam_id) {
        cs = new ConnectWebServer();
        this.student_exam_id = student_exam_id;
        questions = new Questions();
           
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setUndecorated(true);
        setResizable(false);
        this.getContentPane().setBackground(Color.WHITE);
        initComponents();
        
        this.loadNextQuestion();
        //this.loadQuestions();
        this.startTimer();
    }

    ExamPage(StudentExam student_exam, int studentExamID, int studID, int courseID, int subjID, int giID, int userID) {
        this.student_exam = student_exam;
        
        this.student_id = studID;
        this.course_id = courseID;
        this.subject_id = subjID;
        this.gi_id = giID;
        this.user_id = userID;
        this.student_exam_id = studentExamID;
        this.subject_time_limit = student_exam.getSubject_time_limit();
        
        cs = new ConnectWebServer();
        
        questions = new Questions();
           
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setUndecorated(true);
        setResizable(false);
        this.getContentPane().setBackground(Color.WHITE);
        initComponents();
        
        this.loadNextQuestion();
        //this.loadQuestions();
        this.startTimer();
    }
    private static final char NEWLINE = '\n';
    private static final String SPACE_SEPARATOR = " ";
    //if text has \n, \r or \t symbols it's better to split by \s+
    private static final String SPLIT_REGEXP= "\\s+";
    
public static String breakLines(String input, int maxLineLength) {
    String[] tokens = input.split(SPLIT_REGEXP);
    StringBuilder output = new StringBuilder(input.length());
    int lineLen = 0;
    for (int i = 0; i < tokens.length; i++) {
        String word = tokens[i];

        if (lineLen + (SPACE_SEPARATOR + word).length() > maxLineLength) {
            if (i > 0) {
                output.append(NEWLINE);
            }
            lineLen = 0;
        }
        if (i < tokens.length - 1 && (lineLen + (word + SPACE_SEPARATOR).length() + tokens[i + 1].length() <=
                maxLineLength)) {
            word += SPACE_SEPARATOR;
        }
        output.append(word);
        lineLen += word.length();
    }
    return output.toString();
}
    
    private void loadNextQuestion()  {
        //jLabelQuestion.setFont(new Font(jLabelQuestion.getFont().toString(), Font.BOLD, 30));
        //jRadioButtonA.setFont(new Font(jRadioButtonA.getFont().toString(), Font.BOLD, 25));
        //jRadioButtonB.setFont(new Font(jRadioButtonB.getFont().toString(), Font.BOLD, 25));
        //jRadioButtonC.setFont(new Font(jRadioButtonC.getFont().toString(), Font.BOLD, 25));
        //jRadioButtonD.setFont(new Font(jRadioButtonD.getFont().toString(), Font.BOLD, 25));
        try {
            JSONObject jsonobj= cs.getNextQuestion(this.student_exam_id);
            JSONArray questions_array = jsonobj.getJSONArray("question"); 
            System.out.println("absolute.java from server: " + questions_array);
            System.out.println("absolute.java from server: " + questions_array.length());
            
            if(questions_array.length() == 0) {
                stopTimer();

                Float Score = ((float)correctAnswerCount / (float)total) * 100;
                System.out.println(Score + "  " +correctAnswerCount);
                Score score = new Score(this.student_exam, this.student_exam_id);
                score.setVisible(true);
                this.setVisible(false);
                this.dispose();
                return;
            }
            
            this.exam_id = questions_array.getInt(0);
            this.question = questions_array.get(2).toString();
            this.choice_a = questions_array.get(3).toString();
            this.choice_b = questions_array.get(4).toString();
            this.choice_c = questions_array.get(5).toString();
            this.choice_d = questions_array.get(6).toString();
            this.answer = questions_array.get(8).toString();
            this.answer_letter = questions_array.get(7).toString();
            this.question_number = questions_array.getInt(9) + 1;

            jLabelQuestion.setText(this.question_number + ")." + "<html>Hello World!<br>blahblahblah</html>"+ this.question);
            jLabelQuestion.setVisible(false);
            
            jTest.setText(this.question_number + ")." + this.question);
            jTest.setEditable(false);
            jTest.setLineWrap(true);
            jTest.setOpaque(false);
            jTest.setBorder (BorderFactory.createLineBorder (getBackground (), 2));

            //jTextAreaA.setBorder (null);
            jTextAreaA.setText("A). " + this.choice_a);
            jTextAreaA.setEditable(false);
            jTextAreaA.setLineWrap(true);
            jTextAreaA.setOpaque(false);
            jTextAreaB.setEditable(false);
            jTextAreaB.setLineWrap(true);
            jTextAreaB.setOpaque(false);
            jTextAreaB.setBorder (BorderFactory.createLineBorder (getBackground (), 2));
            jTextAreaB.setText("B). " + this.choice_b);
            jTextAreaC.setEditable(false);
            jTextAreaC.setLineWrap(true);
            jTextAreaC.setOpaque(false);
            jTextAreaC.setBorder (BorderFactory.createLineBorder (getBackground (), 2));
            jTextAreaC.setText("C). " + this.choice_c);
            jTextAreaD.setEditable(false);
            jTextAreaD.setLineWrap(true);
            jTextAreaD.setOpaque(false);
            jTextAreaD.setBorder (BorderFactory.createLineBorder (getBackground (), 2));
            jTextAreaD.setText("D). " + this.choice_d);
            

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private void setFontLabel(JLabel jrb) {
            Font labelFont = jrb.getFont();
            String labelText = jrb.getText();

            int stringWidth = jrb.getFontMetrics(labelFont).stringWidth(labelText);
            int componentWidth = jrb.getWidth();

            // Find out how much the font can grow in width.
            double widthRatio = (double)componentWidth / (double)stringWidth;

            int newFontSize = (int)(labelFont.getSize() * widthRatio);
            int componentHeight = jrb.getHeight();

            // Pick a new font size so it will not be larger than the height of label.
            int fontSizeToUse = Math.min(newFontSize, componentHeight);

            // Set the label's font size to the newly determined size.
            jrb.setFont(new Font(labelFont.getName(), Font.BOLD, fontSizeToUse));        
    }
    
    private void setFontRadio(JRadioButton jrb) {
            Font labelFont = jrb.getFont();
            String labelText = jrb.getText();

            int stringWidth = jrb.getFontMetrics(labelFont).stringWidth(labelText);
            int componentWidth = jrb.getWidth();

            // Find out how much the font can grow in width.
            double widthRatio = (double)componentWidth / (double)stringWidth;

            int newFontSize = (int)(labelFont.getSize() * widthRatio);
            int componentHeight = jrb.getHeight();

            // Pick a new font size so it will not be larger than the height of label.
            int fontSizeToUse = Math.min(newFontSize, componentHeight);

            // Set the label's font size to the newly determined size.
            jrb.setFont(new Font(labelFont.getName(), Font.BOLD, fontSizeToUse));        
    }
    
    static int interval;
    static java.util.Timer timer;
    
    private void startTimer() {
        
        int delay = 1000;
        int period = 1000;
        timer = new java.util.Timer();
        interval = this.subject_time_limit - (this.question_number * 60); //Integer.parseInt(120);
        //System.out.println(secs);* 
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                //System.out.println(setInterval());
                int sec = setInterval();
                int min = 0;
                String seconds = "";
                if(sec > 59) {
                    min = (int) (float)sec / 60;
                    sec = (int) (float)sec % 60;
                    seconds = Integer.toString(sec);
                }
                
                if(sec < 10) {
                    seconds = "0" + Integer.toString(sec);
                } else {
                    seconds = Integer.toString(sec);
                }
                
                jLabelTimer.setText("TIMER:      " + min + ":" + seconds + " seconds remaining");
                
            }
        }, delay, period);
        
    }
    
    private void stopTimer() {
        timer.cancel();
    }
    
    private final int setInterval() {
        if (interval == 1) { //finish the exam
            timer.cancel();
        }
            
            
        return --interval;
    }
    
    private void checkAnswer() {
        System.out.println("\n\nCHECK ANSWER: " + this.answer_letter);
        if (jRadioButtonA.isSelected() && this.answer_letter.equals("A")) {
            System.out.println("A correct");
            cs.updateExamQuestion(this.exam_id, 1);
            correctAnswerCount++;

        } else if(jRadioButtonB.isSelected() && this.answer_letter.equals("B")) {
            System.out.println("B correct");
            cs.updateExamQuestion(this.exam_id, 1);
            correctAnswerCount++;

        } else if(jRadioButtonC.isSelected() && this.answer_letter.equals("C")) {
            System.out.println("C correct");
            cs.updateExamQuestion(this.exam_id, 1);
            correctAnswerCount++;

        } else if(jRadioButtonC.isSelected() && this.answer_letter.equals("D")) {
            System.out.println("D correct");
            correctAnswerCount++;
            cs.updateExamQuestion(this.exam_id, 1);
        } else {
            cs.updateExamQuestion(this.exam_id, 0);
        }
        
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelQuestion = new javax.swing.JLabel();
        jLabelTimer = new javax.swing.JLabel();
        jRadioButtonA = new javax.swing.JRadioButton();
        jRadioButtonB = new javax.swing.JRadioButton();
        jRadioButtonC = new javax.swing.JRadioButton();
        jRadioButtonD = new javax.swing.JRadioButton();
        jButtonNext = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaD = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTest = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaB = new javax.swing.JTextArea();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextAreaC = new javax.swing.JTextArea();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextAreaA = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelQuestion.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabelQuestion.setText("Question:");
        getContentPane().add(jLabelQuestion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 1140, 70));

        jLabelTimer.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabelTimer.setForeground(new java.awt.Color(255, 51, 51));
        jLabelTimer.setText("Timer:");
        getContentPane().add(jLabelTimer, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 710, 40));

        jRadioButtonA.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRadioButtonA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonAActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButtonA, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 1040, 70));

        jRadioButtonB.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRadioButtonB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButtonB, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 1040, 70));

        jRadioButtonC.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRadioButtonC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonCActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButtonC, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, 1050, 70));

        jRadioButtonD.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRadioButtonD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonDActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButtonD, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 1040, 70));

        jButtonNext.setFont(new java.awt.Font("Corbel", 0, 36)); // NOI18N
        jButtonNext.setText("NEXT");
        jButtonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNextActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonNext, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 560, -1, -1));
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jTextAreaD.setColumns(20);
        jTextAreaD.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jTextAreaD.setRows(5);
        jTextAreaD.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaD);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 470, 1100, -1));

        jTest.setColumns(20);
        jTest.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jTest.setRows(5);
        jTest.setBorder(null);
        jScrollPane3.setViewportView(jTest);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 1150, 140));

        jTextAreaB.setColumns(20);
        jTextAreaB.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jTextAreaB.setRows(5);
        jScrollPane4.setViewportView(jTextAreaB);

        getContentPane().add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 290, 1100, -1));

        jTextAreaC.setColumns(20);
        jTextAreaC.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jTextAreaC.setRows(5);
        jScrollPane5.setViewportView(jTextAreaC);

        getContentPane().add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 380, 1100, -1));

        jTextAreaA.setColumns(20);
        jTextAreaA.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jTextAreaA.setRows(5);
        jTextAreaA.setBorder(null);
        jScrollPane6.setViewportView(jTextAreaA);

        getContentPane().add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 200, 1100, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonDActionPerformed
        if(jRadioButtonD.isSelected() == false) {
            jRadioButtonD.setSelected(true);
        }
        jRadioButtonB.setSelected(false);
        jRadioButtonC.setSelected(false);
        jRadioButtonA.setSelected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonDActionPerformed

    private void jRadioButtonCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonCActionPerformed
        if(jRadioButtonC.isSelected() == false) {
            jRadioButtonC.setSelected(true);
        }
        jRadioButtonB.setSelected(false);
        jRadioButtonD.setSelected(false);
        jRadioButtonA.setSelected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonCActionPerformed

    private void jRadioButtonBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBActionPerformed
        if(jRadioButtonB.isSelected() == false) {
            jRadioButtonB.setSelected(true);
        }
        jRadioButtonA.setSelected(false);
        jRadioButtonC.setSelected(false);
        jRadioButtonD.setSelected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonBActionPerformed

    private void jRadioButtonAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAActionPerformed
        if(jRadioButtonA.isSelected() == false) {
            jRadioButtonA.setSelected(true);
        }
        jRadioButtonB.setSelected(false);
        jRadioButtonC.setSelected(false);
        jRadioButtonD.setSelected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonAActionPerformed

    private void jButtonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNextActionPerformed
        if(!jRadioButtonA.isSelected() && 
                !jRadioButtonB.isSelected() &&
                !jRadioButtonC.isSelected() && 
                !jRadioButtonD.isSelected()) {
            
            JOptionPane optionPane = new JOptionPane();
            optionPane.setMessage("SELECT ANSWER");
            JDialog dialog = optionPane.createDialog("STATUS");
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);  
        } else {
            if(jButtonNext.getText() == "NEXT") {
                this.checkAnswer();
                this.loadNextQuestion();
                System.out.println("NEXT");
            }
        }
        
        this.resetRadioBtns();
    }//GEN-LAST:event_jButtonNextActionPerformed
    public void resetRadioBtns() {
        jRadioButtonA.setSelected(false);
        jRadioButtonB.setSelected(false);
        jRadioButtonC.setSelected(false);
        jRadioButtonD.setSelected(false);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ExamPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ExamPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ExamPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ExamPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new ExamPage(1).setVisible(true);
            }
        });
    } 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNext;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabelQuestion;
    private javax.swing.JLabel jLabelTimer;
    private javax.swing.JRadioButton jRadioButtonA;
    private javax.swing.JRadioButton jRadioButtonB;
    private javax.swing.JRadioButton jRadioButtonC;
    private javax.swing.JRadioButton jRadioButtonD;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextArea jTest;
    private javax.swing.JTextArea jTextAreaA;
    private javax.swing.JTextArea jTextAreaB;
    private javax.swing.JTextArea jTextAreaC;
    private javax.swing.JTextArea jTextAreaD;
    // End of variables declaration//GEN-END:variables
}
